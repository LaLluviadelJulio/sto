"""STO_server URL Configuration
The `urlpatterns` list routes URLs to views.
"""
from django.contrib import admin
from django.conf import settings
from django.conf.urls import handler404, handler500
from django.conf.urls.static import static

from django.urls import path, include, re_path
from django.views.decorators.cache import cache_page

from STO_main.sitemap import StaticViewSitemap, DynamicCategoryViewSitemap, \
    DynamicStockViewSitemap, DynamicServiceViewSitemap, DynamicCaseViewSitemap
from STO_main.views import MainView, CategoryView, ServiceView, CaseView, CaseListView, StaffListView, \
    VideoReviewsListView, GuaranteeView, ContactUsView, StockView, StockListView, AddServiceToMyListView, \
    RemoveChosenService, AppointmentView, ServicesFilteredView, my_handler404, my_handler500, ReferralProgramView, \
    BonusProgramView, robots_txt

from django.contrib.sitemaps.views import sitemap

sitemaps = {
    'static': StaticViewSitemap,
    'category': DynamicCategoryViewSitemap,
    'services': DynamicServiceViewSitemap,
    'case': DynamicCaseViewSitemap,
    'stocks': DynamicStockViewSitemap,
}

urlpatterns = [
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps},
         name='django.contrib.sitemaps.views.sitemap'),
    path('robots.txt', robots_txt),

    path('admin/', admin.site.urls),
    path('', MainView.as_view(), name='index'),

    path('categories/<slug:category_slug>/', CategoryView.as_view(), name='category_read'),
    path('services/<slug:service_slug>/', ServiceView.as_view(), name='service_read'),
    path('service-add/<slug:slug>/', AddServiceToMyListView.as_view(), name='add_service_to_list'),
    path('service-remove/<slug:slug>/', RemoveChosenService.as_view(), name='remove_service_from_list'),
    path('services-registration/', AppointmentView.as_view(), name='services_registration'),
    path('search/', ServicesFilteredView.as_view(), name='search_services'),

    path('cases/<slug:case_slug>/', CaseView.as_view(), name='case_read'),
    path('cases/', CaseListView.as_view(), name='cases_list'),

    path('stocks/<slug:stock_slug>/', StockView.as_view(), name='stock_detail'),
    path('stocks/', cache_page(60)(StockListView.as_view()), name='stocks_list'),
    # path('stocks/', StockListView.as_view(), name='stocks_list'),
    path('referral/', ReferralProgramView.as_view(), name='referral'),
    path('bonus/', BonusProgramView.as_view(), name='bonus'),

    path('about/', StaffListView.as_view(), name='about'),
    # path('reviews/', VideoReviewsListView.as_view(), name='reviews'),
    path('reviews/', cache_page(60)(VideoReviewsListView.as_view()), name='reviews'),
    path('guarantees/', GuaranteeView.as_view(), name='guarantees'),
    path('contacts/', ContactUsView.as_view(), name='contacts'),
]

handler404 = 'STO_main.views.my_handler404'
handler500 = 'STO_main.views.my_handler500'

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [re_path(r'^__debug_/', include(debug_toolbar.urls))]
    urlpatterns += [path('404/', my_handler404, kwargs={'exception': Exception('Page not found')}, name='page_404')]
    urlpatterns += [path('500/', my_handler500, kwargs={'exception': Exception('Internal server error')},
                         name='page_500')]
