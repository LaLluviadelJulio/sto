"""Contains custom template tags."""

from django import template
from django.core.cache import cache

from ..models import Category
register = template.Library()


@register.simple_tag
def get_categories():
    """Template tag for dynamic display of categories in the main menu."""
    categories = cache.get('categories')
    if not categories:
        categories = Category.objects.filter(is_active=True)
        cache.set('categories', categories, 60)
    return categories
