"""
Contains the mixins necessary to follow the DRY principle and expand the functionality of CBV.
"""
from django.views import View


class TitleMixin(View):
    """The mixin allows you to supplement the views with text titles,
    as well as SEO titles and SEO descriptions."""

    def get_object_title(self, context):
        """Returns as the title of the text the title of the object that has such an attribute."""
        return getattr(self, 'title', '')

    def get_page_title(self, context):
        """Returns as the SEO title of the page the seo_title of the object
        that has such an attribute."""
        return getattr(self, 'seo_title', 'Автосервис Кармастер')

    def get_page_description(self, context):
        """Returns as the SEO description the description of the object
        that has such an attribute."""
        return getattr(self, 'seo_description', '')

    def get_context_data(self, **kwargs):
        """Puts into context the prepared values of the text title,
        the SEO page title and the SEO page description."""
        context = super().get_context_data(**kwargs)
        context['title'] = self.get_object_title(context)
        context['seo_title'] = self.get_page_title(context)
        context['seo_description'] = self.get_page_description(context)
        return context
