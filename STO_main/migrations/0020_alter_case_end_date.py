# Generated by Django 4.1.1 on 2022-10-03 15:38

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('STO_main', '0019_alter_stock_options_alter_videofile_options_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='case',
            name='end_date',
            field=models.DateField(default=datetime.datetime.today, verbose_name='дата окончания работ'),
        ),
    ]
