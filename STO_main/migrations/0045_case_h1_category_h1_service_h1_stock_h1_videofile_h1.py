# Generated by Django 4.1.1 on 2022-10-31 06:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('STO_main', '0044_alter_car_mileage'),
    ]

    operations = [
        migrations.AddField(
            model_name='case',
            name='h1',
            field=models.CharField(blank=True, max_length=150, verbose_name='H1 тэг'),
        ),
        migrations.AddField(
            model_name='category',
            name='h1',
            field=models.CharField(blank=True, max_length=150, verbose_name='H1 тэг'),
        ),
        migrations.AddField(
            model_name='service',
            name='h1',
            field=models.CharField(blank=True, max_length=150, verbose_name='H1 тэг'),
        ),
        migrations.AddField(
            model_name='stock',
            name='h1',
            field=models.CharField(blank=True, max_length=150, verbose_name='H1 тэг'),
        ),
        migrations.AddField(
            model_name='videofile',
            name='h1',
            field=models.CharField(blank=True, max_length=150, verbose_name='H1 тэг'),
        ),
    ]
