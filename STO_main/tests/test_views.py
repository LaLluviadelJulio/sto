"""
Contains unit and integration tests for checking the views of the web application.
"""
import copy
import logging
import sys
from http import HTTPStatus
from io import BytesIO

from PIL import Image
from django.core import mail
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db.models import Q
from django.forms import fields
from django.template.loader import render_to_string
from django.test import TestCase, Client
from django.urls import reverse
from django.utils import dateformat, timezone
from phonenumber_field.formfields import PhoneNumberField

from STO_main.models import Case, Category, Service, Stock, STOStaff, VideoFile, ClientMessages
from STO_server.settings import EMAIL_HOST_USER, DOMAIN_NAME

# disabling logging
if len(sys.argv) > 1 and sys.argv[1] == 'test':
    logging.disable(logging.CRITICAL)


def temporary_image(name='test.jpg'):
    """Creates and returns a test image."""
    bts = BytesIO()
    img = Image.new("RGB", (800, 800))
    img.save(bts, 'jpeg')
    return SimpleUploadedFile(name, bts.getvalue())


class TestBaseViews(TestCase):
    """Base class for testing views."""

    @classmethod
    def setUpClass(cls):
        """Creation of 8 test objects of each type of models.
        The last created object of each type is deactivated."""
        super().setUpClass()

        for number in range(8):
            Case.objects.create(title=f'case_{number}', body=f'Выполнена работа {number}',
                                image_after=temporary_image(), image_before=temporary_image())
            Category.objects.create(title=f'category_{number}')
            Service.objects.create(title=f'service_{number}', body='test',
                                   category=Category.objects.first())
            Stock.objects.create(title=f'stock_{number}',
                                 body=f'Акция {number}', )
            VideoFile.objects.create(title=f'video_{number}',
                                     video=f'https://www.youtube.com/watch?v=_{number}y9x2CHQClTU', )
            STOStaff.objects.create(first_name=f'user_{number}',
                                    last_name=f'lastname_{number}',
                                    position=f'position_{number}', )
            ClientMessages.objects.create(body='Howdy, developer',
                                          user='John Cooper',
                                          create_time=dateformat.format(timezone.now(),
                                                                        'Y-m-d H:i:s'),
                                          phone='88005553535', )

        for item in [Case, Service, Category, Stock, VideoFile, ]:
            obj = item.objects.first()
            obj.seo_title = f'{item.__qualname__}_seo_title'
            obj.seo_description = f'{item.__qualname__}_seo_description'
            obj.save()

        for item in [Case, Service, Category, Stock, STOStaff, VideoFile, ClientMessages]:
            obj = item.objects.last()
            obj.is_active = False
            obj.save()

    def setUp(self):
        """Defining the client for the tests."""
        self.client = Client()

    def test_view_url_exists_at_desired_location(self):
        """Checks that the views URL exists in the desired location."""
        locations = {
            'MainView': '/',
            'CategoryView_all': '/categories/all/',
            'CategoryView_category1': '/categories/category1/',
            'ServiceView': '/services/service1/',
            'CaseListView': '/cases/',
            'CaseView': '/cases/case1/',
            'StockListView': '/stocks/',
            'StockView': '/stocks/stock1/',
            'BonusProgramView': '/bonus/',
            'ReferralProgramView': '/referral/',
            'StaffListView': '/about/',
            'VideoReviewsListView': '/reviews/',
            'GuaranteeView': '/guarantees/',
            'ContactUsView': '/contacts/',
            'AppointmentView': '/services-registration/',
        }

        for view, url in locations.items():
            with self.subTest(url=url):
                response = self.client.get(url)
                self.assertEqual(response.status_code, 200)
        response = self.client.get('/search/?search_panel=serv/',
                                   {'search_panel': 'serv'})
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template_and_categories(self):
        """Checks that the views uses correct template.
        Checks that the correct categories in the context are passed in the menu on each page.
        """
        templates_pages_names = {
            'index.html': reverse('index'),
            'services/categories-list.html': (
                reverse('category_read', kwargs={'category_slug': 'category1'})
            ),
            'services/service-details.html': (
                reverse('service_read', kwargs={'service_slug': 'service1'})
            ),
            'cases/cases-list.html': reverse('cases_list'),
            'cases/case-details.html': (
                reverse('case_read', kwargs={'case_slug': 'case1'})
            ),
            'stocks/stocks-list.html': reverse('stocks_list'),
            'stocks/stock-details.html': (
                reverse('stock_detail', kwargs={'stock_slug': 'stock1'})),
            'stocks/bonus.html': reverse('bonus'),
            'stocks/referral.html': reverse('referral'),

            'about/about.html': reverse('about'),
            'about/reviews-list.html': reverse('reviews'),
            'about/guarantees.html': reverse('guarantees'),

            'about/contact.html': reverse('contacts'),
            'about/appointment.html': reverse('services_registration'),
        }
        categories = Category.objects.filter(is_active=True)
        for template, reverse_name in templates_pages_names.items():
            with self.subTest(reverse_name=reverse_name):
                response = self.client.get(reverse_name)
                self.assertTemplateUsed(response, template)
                self.assertQuerysetEqual(response.context['categories'], categories)

    def test_view_contains_correct_seo_title(self):
        """Checks that the views contains correct seo title."""
        seo_titles = {
            '/': 'Ремонт автомобилей в Севастополе, бесплатная диагностика на СТО «Кармастер».',
            '/categories/all/': 'Услуги СТО "Кармастер" в Севастополе',
            '/categories/category0/': 'Category_seo_title',
            '/services/service0/': 'Service_seo_title',
            '/cases/': 'Наши работы - примеры отремонтированных машин '
                       'на СТО «Кармастер» в Севастополе',
            '/cases/case0/': 'Case_seo_title',
            '/stocks/': 'Акции и скидки на недорогой ремонт автомобилей '
                        'в Севастополе на СТО «Кармастер».',
            '/stocks/stock7/': 'Stock_seo_title',
            '/bonus/': 'Бонусная программа СТО «Кармастер» в Севастополе',
            '/referral/': 'Получите бонусы за привлеченных на СТО клиентов',
            '/about/': 'Официальный сайт СТО «Кармастер» в Севастополе. '
                       'Профессиональный автосервис по доступным ценам.',
            '/reviews/': 'Отзывы о СТО «Кармастер» в Севастополе - мнения клиентов об автосервисе',
            '/guarantees/': 'Ремонт автомобилей в Севастополе с гарантией -  '
                            'СТО «Кармастер» в Севастополе',
            '/contacts/': 'Контактная информация СТО «Кармастер» в Севастополе',
            '/services-registration/': 'Записаться на ремонт автомобиля в '
                                       'Севастополе на СТО «Кармастер».',
        }

        for url, seo_title in seo_titles.items():
            with self.subTest(seo_title=seo_title):
                response = self.client.get(url)
                self.assertEqual(response.context['seo_title'], seo_title)

    def test_view_contains_correct_seo_description(self):
        """Checks that the views contains correct seo description."""
        seo_descriptions = {
            '/': 'СТО «Кармастер» - профессиональный ремонт и '
                 'диагностика автомобилей в Севастополе по доступным ценам.',
            '/categories/all/': 'Ремонт автомобилей недорого в Севастополе',
            '/categories/category0/': 'Category_seo_description',
            '/services/service0/': 'Service_seo_description',
            '/cases/': 'Примеры работ профессионального автосервиса «Кармастер» в Севастополе.',
            '/cases/case0/': 'Case_seo_description',
            '/stocks/': 'СТО «Кармастер» регулярно проводит акции и предоставляет '
                        'скидки на ремонт авто своим клиентам.',
            '/stocks/stock7/': 'Stock_seo_description',
            '/bonus/': 'Условия бонусной программы СТО «Кармастер» ва Севастополе.',
            '/referral/': 'Реферальная программа для клиентов нашего СТО. '
                          'Рекомендуйте нас и получайте бонусы.',
            '/about/': 'Подробная информация об СТО «Кармастер» в Севастополе. '
                       'Профессиональный автосервис по доступным ценам.',
            '/reviews/': 'Отзывы клиентов о СТО «Кармастер» в Севастополе. '
                         'Мнения и впечатления об автосервисе.',
            '/guarantees/': 'Условия гарантии на ремонт автомобилей на  '
                            'СТО «Кармастер» в Севастополе.',
            '/contacts/': 'Контактная информация СТО «Кармастер» в Севастополе. '
                          'Телефоны, адрес, расположение на карте.',
            '/services-registration/': 'Страница для предварительной записи на ремонт '
                                       'или диагностику автомобиля '
                                       'на СТО «Кармастер» в Севастополе.',
        }

        for url, seo_description in seo_descriptions.items():
            with self.subTest(seo_description=seo_description):
                response = self.client.get(url)
                self.assertEqual(response.context['seo_description'], seo_description)

    def test_view_contains_correct_title(self):
        """Checks that the views contains correct title."""
        titles = {
            '/': '',
            '/categories/all/': 'Все категории услуг',
            '/categories/category0/': 'Услуги категории category_0',
            '/services/service0/': 'service_0',
            '/cases/': 'Выполненные работы',
            '/cases/case0/': 'Наша работа: case_0',
            '/stocks/': 'Акции и бонусы',
            '/stocks/stock7/': 'stock_7',
            '/bonus/': 'Бонусная программа от Кармастер',
            '/referral/': 'Реферальная программа от Кармастер',
            '/about/': 'О нас',
            '/reviews/': 'Отзывы о Кармастер',
            '/guarantees/': 'Гарантии Кармастер',
            '/contacts/': 'Связаться с Кармастер',
            '/services-registration/': 'Записаться на СТО',
        }

        for url, title in titles.items():
            with self.subTest(title=title):
                response = self.client.get(url)
                self.assertEqual(response.context['title'], title)

    def test_main_view_last_cases(self):
        """Test that the context of the main page view contains data about recent cases."""
        response = self.client.get('/')
        last_cases = Case.objects.filter(is_active=True)[:6]
        self.assertQuerysetEqual(response.context['last_cases'], last_cases)

    def test_category_view_correct_context_and_queryset(self):
        """Test that the category page view contains correct context and queryset."""
        response = self.client.get(reverse('category_read', kwargs={'category_slug': 'all'}))
        self.assertQuerysetEqual(response.context['object_list'], Service.objects.none())
        self.assertQuerysetEqual(response.context['all'], Category.objects.filter(is_active=True))

        category = Category.objects.get(slug='category1')
        response = self.client.get(reverse('category_read', kwargs={'category_slug': 'category1'}))
        self.assertQuerysetEqual(response.context['object_list'],
                                 Service.objects.filter(Q(category=category), Q(is_active=True)))
        self.assertEqual(response.context['category'], category)

    def test_category_view_paginator(self):
        """Test that category page view contains correct pagination."""
        response = self.client.get(reverse('category_read', kwargs={'category_slug': 'category0'}))
        self.assertEqual(len(response.context['object_list']), 6)
        response = self.client.get(reverse('category_read',
                                           kwargs={'category_slug': 'category0'}) + '?page=2')
        self.assertEqual(len(response.context['object_list']), 1)

    def test_case_list_view_paginator(self):
        """Test that category page list view contains correct pagination."""
        response = self.client.get(reverse('cases_list'))
        self.assertEqual(len(response.context['object_list']), 2)
        response = self.client.get(reverse('cases_list') + '?page=4')
        self.assertEqual(len(response.context['object_list']), 1)

    def test_case_view_contains_correct_context(self):
        """Test that specific case page view contains correct context."""
        response = self.client.get(reverse('case_read', kwargs={'case_slug': 'case1'}))
        self.assertQuerysetEqual(response.context['other_cases'],
                                 Case.objects.filter(is_active=True).exclude(
                                     slug='case1')[:3])

    def test_stock_list_view_paginator(self):
        """Test that stock page list view contains correct pagination."""
        response = self.client.get(reverse('stocks_list'))
        self.assertEqual(len(response.context['object_list']), 3)
        response = self.client.get(reverse('stocks_list') + '?page=3')
        self.assertEqual(len(response.context['object_list']), 1)

    def test_stock_view__correct_context(self):
        """Test that specific stock page view contains correct context."""
        response = self.client.get(reverse('stock_detail', kwargs={'stock_slug': 'stock1'}))
        self.assertQuerysetEqual(response.context['other_stocks'],
                                 Stock.objects.filter(is_active=True).exclude(slug='stock1')[:3])

    def test_staff_list_view_contains_correct_queryset(self):
        """Test that about page view contains correct STOStaff queryset."""
        response = self.client.get(reverse('about'))
        self.assertQuerysetEqual(response.context['object_list'],
                                 STOStaff.objects.filter(is_active=True))

    def test_video_reviews_list_view_paginator(self):
        """Test that video reviews page list view contains correct pagination."""
        response = self.client.get(reverse('reviews'))
        self.assertEqual(len(response.context['object_list']), 2)
        response = self.client.get(reverse('reviews') + '?page=4')
        self.assertEqual(len(response.context['object_list']), 1)

    def test_services_filtered_view(self):
        """Test for the view for searching for a service by part of a phrase.
        A performance test for searching for a service with a given empty phrase. Pagination test.
        Test of transmitting the last search phrase to the session.
        """
        response = self.client.get('/search/?search_panel=serv/',
                                   {'search_panel': ''})
        self.assertEqual(response.status_code, 200)
        query = Service.objects.filter(title__icontains='serv', is_active=True)[:6]
        self.assertQuerysetEqual(response.context['object_list'], query, ordered=False)
        self.assertEqual(response.context['title'], 'Чтобы выполнить поиск, '
                                                    'задайте поисковую фразу')
        self.assertEqual(self.client.session['filter'], '')

        response = self.client.get('/search/?search_panel=serv/',
                                   {'search_panel': 'serv'})
        self.assertEqual(response.status_code, 200)
        query = Service.objects.filter(title__icontains='serv', is_active=True)[:6]
        self.assertQuerysetEqual(response.context['object_list'], query, ordered=False)
        self.assertEqual(response.context['title'], 'Поиск услуг по фразе "serv"')
        self.assertEqual(self.client.session['filter'], 'serv')
        self.assertTemplateUsed(response, 'services/service-filtered.html')

        response = self.client.get('/search/?page=2', )
        self.assertEqual(len(response.context['object_list']), 1)

    def test_contact_us_view(self):
        """Test for the view of contacts page.
        A test of the correct correspondence of the form fields.
        A test of the correct transmission of the completed form data.
        A test for the output of form errors and message
        when the form is filled out correctly and the message is sent.
        Test whether the email was sent.
        """
        response = self.client.get(reverse('contacts'))
        form_fields = {
            'content': fields.CharField,
            'name': fields.CharField,
            'phone': PhoneNumberField,
        }
        for value, expected in form_fields.items():
            with self.subTest(value=value):
                form_field = response.context.get('form').fields.get(value)
                self.assertIsInstance(form_field, expected)
        data = {
            'content': 'Привет',
            'name': 'Chester Bennington',
            'phone': '88005553535',
        }
        response = self.client.post(path='/contacts/', data=data)
        self.assertEqual(response.status_code, 200)
        messages = response.context['messages']
        for message in messages:
            self.assertEqual(str(message), 'Ваше сообщение успешно отправлено.'
                                           'Мы свяжемся с вами в ближайшее время.')

        context = {'name': 'Chester Bennington', 'my_site_name': DOMAIN_NAME,
                   'content': 'Привет', 'phone': '88005553535',
                   'form_date': timezone.now(), 'form_type': 'обратной связи',
                   }
        message = render_to_string('new-appointment-email.html', context)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Новое сообщение от клиента для Кармастер92')
        self.assertEqual(mail.outbox[0].from_email, EMAIL_HOST_USER)
        self.assertEqual(mail.outbox[0].to[0], EMAIL_HOST_USER)
        self.assertEqual(mail.outbox[0].body, message)

        for item in data:
            datacopy = copy.deepcopy(data)
            datacopy[item] = ''
            response = self.client.post(path='/contacts/', data=datacopy)
            self.assertFormError(response, 'form', item, 'Обязательное поле.')
        data['phone'] = 'sdfsdf'
        response = self.client.post(path='/contacts/', data=data)
        self.assertFormError(response, 'form', 'phone', 'Введите корректный номер телефона '
                                                        '(например, 8 (301) 123-45-67) или номер '
                                                        'с префиксом международной связи.')

    def test_appointment_view(self):
        """Test for the view of appointment (pre-registration for a service station).
        Test whether the message was sent.
        Test of receiving services from the session that the user has marked
        as interesting to him and adding their list to the email.
        """
        session = self.client.session
        session['user_services'] = [item.slug for item in Service.objects.all()]
        session.save()

        response = self.client.get(reverse('services_registration'))
        self.assertEqual(response.context['services_sum'], 12000.00)

        self.assertQuerysetEqual(response.context['services'], Service.objects.all())

        data = {
            'content': 'Привет',
            'name': 'Chester Bennington',
            'phone': '88005553535',
        }

        response = self.client.post(path='/services-registration/', data=data, )
        self.assertEqual(response.status_code, 200)

        context = {'name': 'Chester Bennington', 'my_site_name': DOMAIN_NAME,
                   'content': 'Привет', 'phone': '88005553535',
                   'form_date': timezone.now(), 'form_type': 'обратной связи',
                   }
        message = render_to_string('new-appointment-email.html', context)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Новое сообщение от клиента для Кармастер92')
        self.assertEqual(mail.outbox[0].from_email, EMAIL_HOST_USER)
        self.assertEqual(mail.outbox[0].to[0], EMAIL_HOST_USER)
        self.assertEqual(mail.outbox[0].body, message)


class RobotsTxtTests(TestCase):
    def test_get(self):
        response = self.client.get("/robots.txt")

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(response["content-type"], "text/plain")
        lines = response.content.decode().splitlines()
        self.assertEqual(lines[0], "User-Agent: *")

    def test_post_disallowed(self):
        response = self.client.post("/robots.txt")

        self.assertEqual(HTTPStatus.METHOD_NOT_ALLOWED, response.status_code)
