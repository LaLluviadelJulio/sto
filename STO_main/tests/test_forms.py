"""
Contains unit and integration tests for checking the forms of the web application.
"""
import logging
import sys

from django.test import TestCase

from ..forms import ContactUsForm

# disabling logging
if len(sys.argv) > 1 and sys.argv[1] == 'test':
    logging.disable(logging.CRITICAL)

class TestContactUsForm(TestCase):
    """testing a form for a new message from a site user
    (a question or a preliminary appointment at a service station)."""

    def __init__(self, *args, **kwargs):
        """Preparation of data for tests."""
        super().__init__(*args, **kwargs)
        self.form_data = {
            'content': 'Здравствуйте, я хотел бы записаться на СТО, у меня проблема с шрус',
            'name': 'Quentin Tarantino',
            'phone': '88005553535',
        }

    def test_register_form_fields_placeholders(self):
        """Testing of placeholders filling in the contact form fields."""
        form = ContactUsForm()
        self.assertTrue(form.fields['name'].widget.attrs['placeholder'] ==
                        'Введите ваше ФИО')
        self.assertTrue(form.fields['content'].widget.attrs['placeholder'] ==
                        'Введите текст сообщения')
        self.assertTrue(form.fields['phone'].widget.attrs['placeholder'] ==
                        'Введите номер телефона')
