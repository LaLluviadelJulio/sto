"""
Contains unit and integration tests for checking the models of the web application.
"""

import logging
import sys
from io import BytesIO

from PIL import Image
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db import IntegrityError
from django.test import TestCase
from django.utils import timezone, dateformat

from ..models import Category, Service, Case, VideoFile, Stock, Car, STOStaff, ClientMessages

# disabling logging
if len(sys.argv) > 1 and sys.argv[1] == 'test':
    logging.disable(logging.CRITICAL)


def temporary_image(name='test.jpg'):
    """Creates and returns a test image."""
    bts = BytesIO()
    img = Image.new("RGB", (800, 800))
    img.save(bts, 'jpeg')
    return SimpleUploadedFile(name, bts.getvalue())


class TestBaseModel(TestCase):
    """Base class for testing model."""
    model = Category

    @staticmethod
    def create_category(title='Skillet'):
        """Returns the created category."""
        return Category.objects.create(title=title)

    @classmethod
    def create_services(cls):
        """Returns a list of created services."""
        service_01 = Service.objects.create(title='Monster',
                                            body="The secret side of me I never let you "
                                                 "see; I keep it caged but I can't control it...")
        service_02 = Service.objects.create(title='Hero',
                                            body="I'm just a step away, I'm just a breath away. "
                                                 "Losin' my faith today (I'm falling off the "
                                                 "edge today)")
        cls.create_category()
        service_01.category = Category.objects.first()
        service_02.category = Category.objects.first()

        service_01.save()
        service_02.save()
        return [service_01, service_02]

    @classmethod
    def setUpClass(cls):
        """Creates a class model objects."""
        super().setUpClass()
        cls.test_obj_01 = cls.model.objects.create(
            title='Bands')
        cls.test_obj_02 = cls.model.objects.create(
            title='Skillet')
        if cls.model == Category:
            cls.test_obj_02.parent = cls.test_obj_01
            cls.test_obj_02.save()

    def test_verbose_name(self):
        """Checks that the verbose_name value in the model fields is the same as expected."""
        test_obj_01 = self.test_obj_01
        field_verboses = {
            'seo_title': 'SEO title',
            'seo_description': 'SEO description',
            'title': 'наименование',
            'create_time': 'время создания',
            'update_time': 'время изменения',
            'slug': 'URL',
            'is_active': 'активен',
        }
        for field, expected_value in field_verboses.items():
            with self.subTest(field=field):
                self.assertEqual(
                    test_obj_01._meta.get_field(field).verbose_name, expected_value)

    def test_string_output_of_the_model(self):
        """Model string output text test."""
        test_obj_01 = self.test_obj_01
        expected_string_output = test_obj_01.title
        self.assertEqual(expected_string_output, str(test_obj_01))

    def test_autofill_slug_field(self, name_01='bands', name_02='skillet'):
        """Checking the slug field for autofill and modification."""
        test_obj_01 = self.test_obj_01
        test_obj_02 = self.test_obj_02

        self.assertEqual(test_obj_01.slug, name_01)
        self.assertEqual(test_obj_02.slug, name_02)

        test_obj_02.title = 'Red'
        test_obj_02.save()
        self.assertEqual(test_obj_02.slug, 'red')

        test_obj_01.title = 'Red'
        with self.assertRaises(IntegrityError):
            test_obj_01.save()
        self.assertNotEqual(test_obj_01.slug, 'red')
        test_obj_01.title, test_obj_02.title = name_01.capitalize(), name_02.capitalize()
        test_obj_01.save()
        test_obj_02.save()

    def test_autofill_update_time_field(self):
        """Checking the update_time field for autofill and modification."""
        test_obj_01 = self.test_obj_01
        start_update_time = test_obj_01.update_time
        test_obj_01.save()
        self.assertNotEqual(start_update_time, test_obj_01.update_time)


class TestCategoryModel(TestBaseModel):
    """Test for the Category model."""
    def test_verbose_name(self):
        """Checks if the verbose_name value in the Category model fields is the same as expected."""
        super().test_verbose_name()
        test_obj_01 = TestCategoryModel.test_obj_01
        field_verboses = {
            'description': 'описание',
            'image': 'картинка',
            'parent': 'родительская категория',
        }
        for field, expected_value in field_verboses.items():
            with self.subTest(field=field):
                self.assertEqual(
                    test_obj_01._meta.get_field(field).verbose_name, expected_value)

    #
    def test_child_categories(self):
        """Checks the deactivation of child categories when the parent category is deactivated.
        Checks the activation of child categories when the parent category is activated.
        """
        parent_category = TestCategoryModel.test_obj_01
        child_category = TestCategoryModel.test_obj_02
        child_category.parent = parent_category
        child_category.save()
        [self.assertTrue(i.is_active) for i in (parent_category, child_category)]

        parent_category.is_active = False
        parent_category.save()
        child_category.refresh_from_db()
        [self.assertFalse(i.is_active) for i in (parent_category, child_category)]

        parent_category.is_active = True
        parent_category.save()
        child_category.refresh_from_db()
        [self.assertTrue(i.is_active) for i in (parent_category, child_category)]

    def test_related_services(self):
        """Checks the deactivation of child services when the parent category is deactivated.
        Checks the activation of child services when the parent category is activated.
        """
        subcategory = TestCategoryModel.test_obj_02
        related_service_01 = Service.objects.create(
            title='Monster',
            category=subcategory,
            body="The secret side of me I never let you see; I keep it caged "
                 "but I can't control it...", )
        related_service_02 = Service.objects.create(
            title='Hero',
            category=subcategory,
            body="I'm just a step away, I'm just a breath away. Losin' my faith today"
                 "(I'm falling off the edge today)", )

        [self.assertTrue(i.is_active) for i in (subcategory, related_service_01,
                                                related_service_02)]

        subcategory.is_active = False
        subcategory.save()
        related_service_01.refresh_from_db()
        related_service_02.refresh_from_db()
        [self.assertFalse(i.is_active) for i in (subcategory, related_service_01,
                                                 related_service_02)]

        subcategory.is_active = True
        subcategory.save()
        related_service_01.refresh_from_db()
        related_service_02.refresh_from_db()
        [self.assertTrue(i.is_active) for i in (subcategory, related_service_01,
                                                related_service_02)]

    def test_image_path_and_size(self):
        """Checks the path and size of the saved model image."""
        test_obj_01 = TestCategoryModel.test_obj_01
        test_obj_01.image = temporary_image()

        test_obj_01.save()
        title_str = test_obj_01.title.replace(' ', '_')
        self.assertEqual(test_obj_01.image.url, f'/media/cat_images/{title_str}_test.jpg')

        self.assertEqual(test_obj_01.image.height, 240)
        self.assertEqual(test_obj_01.image.width, 240)

        test_obj_01.image.delete(save=False)


class TestServiceModel(TestBaseModel):
    """Test for the Service model."""
    model = Service

    @classmethod
    def setUpClass(cls):
        """Set up non-modified Service objects used by all test methods."""
        super().setUpClass()

        cls.create_category()
        cls.test_obj_01.title = 'Monster'
        cls.test_obj_01.category = Category.objects.first()
        cls.test_obj_01.body = \
            "The secret side of me I never let you see; I keep it caged but I can't control it..."
        cls.test_obj_01.save()

        cls.test_obj_02.title = 'Hero'
        cls.test_obj_02.category = Category.objects.first()
        cls.test_obj_02.body = \
            "I'm just a step away, I'm just a breath away. Losin' my faith today" \
            "(I'm falling off the edge today)"
        cls.test_obj_02.save()

    def test_verbose_name(self):
        """Checks if the verbose_name value in the fields is the same as expected."""
        super().test_verbose_name()
        test_obj_01 = TestServiceModel.test_obj_01
        field_verboses = {
            'body': 'описание услуги',
            'category': 'категория',
            'image': 'картинка',
            'min_price': 'мин. стоимость',
            'max_price': 'макс. стоимость',
        }
        for field, expected_value in field_verboses.items():
            with self.subTest(field=field):
                self.assertEqual(
                    test_obj_01._meta.get_field(field).verbose_name, expected_value)

    def test_autofill_slug_field(self, name_01='monster', name_02='hero'):
        """Checking the slug field for autofill and modification."""
        super().test_autofill_slug_field(name_01=name_01, name_02=name_02)

    #
    def test_image_path_and_size(self):
        """Checks the path and size of the saved model image."""
        test_obj_01 = TestServiceModel.test_obj_01
        test_obj_01.image = temporary_image()

        test_obj_01.save()
        self.assertEqual(test_obj_01.image.url,
                         f'/media/service_images/{test_obj_01.category}/{test_obj_01}_test.jpg')

        self.assertEqual(test_obj_01.image.height, 240)
        self.assertEqual(test_obj_01.image.width, 240)

        test_obj_01.image.delete(save=False)


class TestCaseModel(TestBaseModel):
    """Test for the Case model."""
    model = Case

    @classmethod
    def setUpClass(cls):
        """Set up non-modified Case objects used by all test methods."""
        super().setUpClass()
        services = cls.create_services()
        cls.test_obj_01 = Case.objects.create(title='Awake',
                                              body='Альбом 2009 года')
        cls.test_obj_02 = Case.objects.create(title='Best',
                                              body='Сборник лучших рок-треков 2005-2022')
        cls.test_obj_01.services.add(*services)
        cls.test_obj_02.services.add(*services)
        cls.test_obj_01.save()
        cls.test_obj_02.save()

    def test_verbose_name(self):
        """Checks if the verbose_name value in the fields is the same as expected."""
        super().test_verbose_name()
        test_obj_01 = TestCaseModel.test_obj_01
        field_verboses = {
            'body': 'описание',
            'end_date': 'дата окончания работ',
            'services': 'задействованные услуги',
            'image_before': 'фото до',
            'image_after': 'фото после',
            'price': 'стоимость',
            'period': 'длительность работ в днях',
            'car': 'связанный автомобиль',
        }
        for field, expected_value in field_verboses.items():
            with self.subTest(field=field):
                self.assertEqual(
                    test_obj_01._meta.get_field(field).verbose_name, expected_value)

    def test_string_output_of_the_model(self):
        """Case model string output text test."""
        test_obj_01 = TestCaseModel.test_obj_01
        expected_string_output = f'{test_obj_01.title} | {test_obj_01.end_date}'
        self.assertEqual(expected_string_output, str(test_obj_01))

    def test_autofill_slug_field(self, name_01='awake', name_02='best'):
        """Checking the slug field for autofill and modification."""
        super().test_autofill_slug_field(name_01=name_01, name_02=name_02)

    def test_image_path_and_size(self):
        """Checks the path and size of the saved model image."""
        test_obj_01 = TestCaseModel.test_obj_01
        test_obj_01.image_before = temporary_image()
        test_obj_01.image_after = temporary_image(name='test_2.jpg')

        test_obj_01.save()
        title_str = test_obj_01.title.replace(' ', '_')
        self.assertEqual(test_obj_01.image_before.url, f'/media/case_images/{title_str}_test.jpg')
        self.assertEqual(test_obj_01.image_after.url, f'/media/case_images/{title_str}_test_2.jpg')

        for image in [test_obj_01.image_before, test_obj_01.image_after]:
            self.assertEqual(image.height, 484)
            self.assertEqual(image.width, 484)
            image.delete(save=False)

    def test_period_to_days(self):
        """Checks the correct formation of the declension of
        the word "день" depending on quantitative characteristics."""
        test_obj_01 = TestCaseModel.test_obj_01
        for period in [11, 15, 45, 116, 1013, 1011]:
            test_obj_01.period = period
            test_obj_01.save()
            self.assertEqual('дней', test_obj_01.period_to_days())
        for period in [2, 3, 4, 24, 43, 482, 102, 1002]:
            test_obj_01.period = period
            test_obj_01.save()
            self.assertEqual('дня', test_obj_01.period_to_days())
        for period in [1, 21, 101, 561, 1001]:
            test_obj_01.period = period
            test_obj_01.save()
            self.assertEqual('день', test_obj_01.period_to_days())


class TestVideoFileModel(TestBaseModel):
    """Test for the VideoFile model."""
    model = VideoFile

    @classmethod
    def setUpClass(cls):
        """Set up non-modified VideoFile objects used by all test methods."""
        super().setUpClass()
        services = cls.create_services()
        cls.test_case = Case.objects.create(title='Awake',
                                            body='Альбом 2009 года')
        cls.test_case.services.add(*services)
        cls.test_case.save()
        cls.test_obj_01 = VideoFile.objects.create(title='Hero',
                                                   video='https://www.youtube.com/watch?v=y9x2CHQClTU',
                                                   related_case=cls.test_case)

    def test_verbose_name(self):
        """The verbose_name value in the fields is the same as expected."""
        super().test_verbose_name()
        test_obj_01 = TestVideoFileModel.test_obj_01
        field_verboses = {
            'video': 'ссылка на видео на YouTube',
            'description': 'описание',
            'related_case': 'связанная работа',
            'person': 'хозяин авто (автор отзыва)',
        }
        for field, expected_value in field_verboses.items():
            with self.subTest(field=field):
                self.assertEqual(
                    test_obj_01._meta.get_field(field).verbose_name, expected_value)

    def test_autofill_slug_field(self, name_01='hero', name_02='name_02'):
        """Checking the slug field for autofill and modification."""
        super().test_autofill_slug_field(name_01=name_01)

    def test_video_link_processing(self):
        """Video link processing test for its correct display on the page."""
        test_obj_01 = TestVideoFileModel.test_obj_01
        test_obj_01.video = 'https://www.youtube.com/watch?v=y9x2CHQClTU'
        test_obj_01.save()
        self.assertEqual('https://www.youtube.com/embed/y9x2CHQClTU', test_obj_01.video)


class TestStockModel(TestBaseModel):
    """Test for the Stock model."""
    model = Stock

    @classmethod
    def setUpClass(cls):
        """Set up non-modified Stock objects used by all test methods."""
        super().setUpClass()
        services = cls.create_services()
        cls.test_obj_01 = Stock.objects.create(title='Concert',
                                               body='Minsk 16.11.2019', )
        cls.test_obj_01.services.add(*services)
        cls.test_obj_01.save()

    def test_verbose_name(self):
        """The verbose_name value in the fields is the same as expected."""
        super().test_verbose_name()
        test_obj_01 = TestStockModel.test_obj_01
        field_verboses = {
            'body': 'описание',
            'conditions': 'условия',
            'start_date': 'дата начала',
            'end_date': 'дата окончания',
            'image': 'фон баннера',
            'services': 'услуги, на которые акция',
        }
        for field, expected_value in field_verboses.items():
            with self.subTest(field=field):
                self.assertEqual(
                    test_obj_01._meta.get_field(field).verbose_name, expected_value)

    def test_autofill_slug_field(self, name_01='concert', name_02='name_02'):
        """Checking the slug field for autofill and modification."""
        super().test_autofill_slug_field(name_01=name_01)

    def test_indefinite_stock(self):
        """Test for deleting the values of the start and end dates
        of the promotion, if it is noted that it is indefinite."""
        test_obj_01 = TestStockModel.test_obj_01
        test_obj_01.is_indefinite = True
        test_obj_01.save()
        self.assertIsNone(test_obj_01.start_date)
        self.assertIsNone(test_obj_01.end_date)

    def test_image_path_and_size(self):
        """Checks the path and size of the saved model image."""
        test_obj_01 = TestStockModel.test_obj_01
        test_obj_01.image = temporary_image()
        test_obj_01.save()

        title_str = test_obj_01.title.replace(' ', '_')
        self.assertEqual(test_obj_01.image.url, f'/media/stock_images/{title_str}_test.jpg')

        self.assertEqual(test_obj_01.image.height, 300)
        self.assertEqual(test_obj_01.image.width, 300)
        test_obj_01.image.delete(save=False)


class TestCarModel(TestCase):
    """Test for the Car model."""
    @classmethod
    def setUpClass(cls):
        """Set up non-modified Car objects used by all test methods."""
        super().setUpClass()
        cls.test_obj_01 = Car.objects.create(brand='Chevrolet',
                                             car_model='Orlando', )

    def test_verbose_name(self):
        """The verbose_name value in the fields is the same as expected."""
        test_obj_01 = TestCarModel.test_obj_01
        field_verboses = {
            'brand': 'марка автомобиля',
            'car_model': 'модель автомобиля',
            'vin': 'VIN автомобиля',
            'release_year': 'год выпуска',
            'licence_plate': 'номера автомобиля',
            'description': 'дополнительная информация об автомобиле',
            'mileage': 'пробег',
            'is_active': 'активен',
            'create_time': 'время создания',
            'update_time': 'время изменения',
        }
        for field, expected_value in field_verboses.items():
            with self.subTest(field=field):
                self.assertEqual(
                    test_obj_01._meta.get_field(field).verbose_name, expected_value)

    def test_string_output_of_the_model(self):
        """Car model string output text test."""
        test_obj_01 = TestCarModel.test_obj_01
        expected_string_output = f'{test_obj_01.brand} {test_obj_01.car_model}'
        self.assertEqual(expected_string_output, str(test_obj_01))

    def autofill_update_time_field(self):
        """Checking the slug field for autofill and modification."""
        test_obj_01 = TestCarModel.test_obj_01
        start_update_time = test_obj_01.update_time
        test_obj_01.save()
        self.assertNotEqual(start_update_time, test_obj_01.update_time)


class TestSTOStaffModel(TestCase):
    """Test for the STOStaff model."""
    @classmethod
    def setUpClass(cls):
        """Set up non-modified STOStaff objects used by all test methods."""
        super().setUpClass()
        cls.test_obj_01 = STOStaff.objects.create(first_name='John',
                                                  last_name='Cooper',
                                                  position='lead vocals, bass', )

    def test_verbose_name(self):
        """The verbose_name value in the fields is the same as expected."""
        test_obj_01 = TestSTOStaffModel.test_obj_01
        field_verboses = {
            'create_time': 'время создания',
            'update_time': 'время изменения',
            'is_active': 'активен',
            'first_name': 'имя',
            'last_name': 'фамилия',
            'patronymic': 'отчество',
            'position': 'должность',
            'range': 'определение порядка вывода',
            'about': 'о сотруднике',
            'image': 'фото сотрудника',
        }
        for field, expected_value in field_verboses.items():
            with self.subTest(field=field):
                self.assertEqual(
                    test_obj_01._meta.get_field(field).verbose_name, expected_value)

    def test_string_output_of_the_model(self):
        """STOStaff model string output text test."""
        test_obj_01 = TestSTOStaffModel.test_obj_01
        expected_string_output = f'{test_obj_01.last_name} {test_obj_01.first_name}'
        self.assertEqual(expected_string_output, str(test_obj_01))

    def autofill_update_time_field(self):
        """Checking the slug field for autofill and modification."""
        test_obj_01 = TestSTOStaffModel.test_obj_01
        start_update_time = test_obj_01.update_time
        test_obj_01.save()
        self.assertNotEqual(start_update_time, test_obj_01.update_time)

    def test_image_path_and_size(self):
        """Checks the path and size of the saved model image."""
        test_obj_01 = TestSTOStaffModel.test_obj_01
        test_obj_01.image = temporary_image()
        test_obj_01.save()
        self.assertEqual(test_obj_01.image.url,
                         f'/media/staff_images/{test_obj_01.first_name}_'
                         f'{test_obj_01.last_name}_test.jpg')

        self.assertEqual(test_obj_01.image.height, 350)
        self.assertEqual(test_obj_01.image.width, 350)
        test_obj_01.image.delete(save=False)


class TestClientMessagesModel(TestCase):
    """Test for the ClientMessages model."""
    @classmethod
    def setUpClass(cls):
        """Set up non-modified ClientMessages objects used by all test methods."""
        super().setUpClass()
        cls.test_obj_01 = ClientMessages.objects.create(body='Howdy, developer',
                                                        user='John Cooper',
                                                        create_time=dateformat.format(timezone.now(), 'Y-m-d H:i:s'),
                                                        phone='88005553535',)

    def test_verbose_name(self):
        """The verbose_name value in the fields is the same as expected."""
        test_obj_01 = TestClientMessagesModel.test_obj_01
        field_verboses = {
            'create_time': 'время создания',
            'update_time': 'время изменения',
            'body': 'содержимое сообщения',
            'user': 'имя пользователя',
            'phone': 'номер телефона',
            'processed': 'обработан',
            'chosen_services': 'интересующие услуги',
        }
        for field, expected_value in field_verboses.items():
            with self.subTest(field=field):
                self.assertEqual(
                    test_obj_01._meta.get_field(field).verbose_name, expected_value)

    def test_string_output_of_the_model(self):
        """ClientMessages model string output text test."""
        test_obj_01 = TestClientMessagesModel.test_obj_01
        expected_string_output = f'Сообщение пользователя ' \
                                 f'{test_obj_01.user} от {test_obj_01.create_time}'
        self.assertEqual(expected_string_output, str(test_obj_01))

    def autofill_update_time_field(self):
        """Checking the slug field for autofill and modification."""
        test_obj_01 = TestClientMessagesModel.test_obj_01
        start_update_time = test_obj_01.update_time
        test_obj_01.save()
        self.assertNotEqual(start_update_time, test_obj_01.update_time)

    def test_processing_phone_format(self):
        """A test of processing the phone number entered by the user
        to bring it to an international format.."""
        test_obj_01 = TestClientMessagesModel.test_obj_01
        test_obj_01.phone = '+780055536363'
        test_obj_01.save()
        self.assertNotEqual(test_obj_01.phone, '880055536363')
