"""Contains custom commands for easy launch by manage.py."""
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):
    """A command for quickly creating a superuser."""
    def handle(self, *args, **options):
        user = User.objects.create_superuser('karmaster92', 'KMsevas@yandex.ru', 'Ilovecars92)')
        user.is_active = True
        user.save()
