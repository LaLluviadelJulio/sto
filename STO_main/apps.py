"""Automatically created by Django to configure the web application."""
from django.apps import AppConfig


class StoConfig(AppConfig):
    """STO_main app configuration, automatically created Django class."""
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'STO_main'
