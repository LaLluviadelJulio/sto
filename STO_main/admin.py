"""
Settings for convenient and correct operation of the admin panel.
"""

from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html
from django.utils.http import urlencode
from django_mptt_admin.admin import DjangoMpttAdmin

from STO_main.models import Category, Service, Case, STOStaff, VideoFile, Stock, Car, ClientMessages

admin.site.site_header = 'Админ-панель СТО "Кармастер"'


class CategoryAdmin(DjangoMpttAdmin):
    """A class for working with the Category model in the admin panel."""
    list_display = ('title', 'parent', 'is_active')
    search_fields = ('title',)
    list_filter = ('is_active',)
    fields = (('seo_title', 'seo_description', 'h1',), ('title', 'is_active'),
              'description', 'image', 'parent',)


class CaseAdmin(admin.ModelAdmin):
    """A class for working with the Case model in the admin panel."""
    list_display = ('id', 'title', 'end_date', 'is_active', 'view_services_link',
                    'view_videofiles_link', 'view_car_link',)
    list_display_links = ('title',)
    search_fields = ('title',)
    list_filter = ('is_active', 'end_date',)
    fields = (('seo_title', 'seo_description', 'h1',), ('title', 'is_active', 'body',),
              ('period', 'end_date',), ('price',), ('image_before', 'image_after',),
              'services', 'car',)

    def view_services_link(self, obj: Case):
        """ A class for creating a table list field with services related to a case
        (which have been performed within the scope of this order).
        """
        count = obj.services.count()
        url = (reverse("admin:STO_main_service_changelist")
               + "?" + urlencode({"case__id": f"{obj.id}"}))
        return format_html('<a href="{}">Услуг в заказе: {}</a>', url, count)

    view_services_link.short_description = "Связанные услуги"

    def view_videofiles_link(self, obj: Case):
        """ A class for creating a table list field with videofiles related to a case
        (which have been performed within the scope of this order).
        """
        count = obj.videofile_set.count()
        url = (reverse("admin:STO_main_videofile_changelist")
               + "?" + urlencode({"related_case__id": f"{obj.id}"}))
        return format_html('<a href="{}">Отзывов на работу: {}</a>', url, count)

    view_videofiles_link.short_description = "Связанные отзывы"

    def view_car_link(self, obj: Case):
        """ A class for creating a table list field with car related to a case
        (which have been performed within the scope of this order).
        """
        url = (reverse("admin:STO_main_car_changelist")
               + "?" + urlencode({"related_case__id": f"{obj.id}"}))
        return format_html('<a href="{}">{}</a>', url, obj.car)

    view_car_link.short_description = "Автомобиль"


class ServiceAdmin(admin.ModelAdmin):
    """A class for working with the Service model in the admin panel."""
    list_display = ('id', 'title', 'category', 'view_cases_link', 'view_stocks_link', 'is_active')
    list_display_links = ('title',)
    search_fields = ('title',)
    list_filter = ('is_active',)
    fields = (('seo_title', 'seo_description', 'h1',), ('title', 'category', 'is_active',),
              ('min_price', 'max_price',), 'body', 'image',)

    def view_cases_link(self, obj: Service):
        """ A class for creating a table list field with cases related to a service.
        """
        count = obj.case_set.count()
        url = (reverse("admin:STO_main_case_changelist")
               + "?" + urlencode({"services__id": f"{obj.id}"}))
        return format_html('<a href="{}">Заказов c этой услугой: {}</a>', url, count)

    view_cases_link.short_description = "Связанные заказы"

    def view_stocks_link(self, obj: Service):
        """ A class for creating a table list field with stocks related to a service.
        """
        count = obj.stock_set.count()
        url = (reverse("admin:STO_main_stock_changelist")
               + "?" + urlencode({"services__id": f"{obj.id}"}))
        return format_html('<a href="{}">Акций с этой услугой: {}</a>', url, count)

    view_stocks_link.short_description = "Связанные акции"


class StockAdmin(admin.ModelAdmin):
    """A class for working with the Stock model in the admin panel."""
    list_display = ('id', 'title', 'is_indefinite', 'start_date',
                    'end_date', 'is_active', 'view_services_link')
    list_display_links = ('title',)
    search_fields = ('title', 'start_date', 'end_date',)
    list_filter = ('is_active', 'start_date', 'end_date', 'is_indefinite',)
    fields = (('seo_title', 'seo_description', 'h1',), ('title', 'is_active',), ('body', 'conditions',),
              ('is_indefinite',), ('start_date', 'end_date',),
              ('image',), 'services',)

    def view_services_link(self, obj: Stock):
        """ A class for creating a table list field with services related to a stock.
        """
        count = obj.services.count()
        url = (reverse("admin:STO_main_service_changelist")
               + "?" + urlencode({"stock__id": f"{obj.id}"}))
        return format_html('<a href="{}">Услуг в акции: {}</a>', url, count)

    view_services_link.short_description = "Связанные услуги"


class VideoFileAdmin(admin.ModelAdmin):
    """A class for working with the VideoFile model in the admin panel."""
    list_display = ('id', 'title', 'is_active', 'view_case_link')
    list_display_links = ('title',)
    search_fields = ('title',)
    list_filter = ('is_active', 'create_time',)
    fields = (('seo_title', 'seo_description', 'h1',), ('title', 'is_active',), 'description', 'person',
              'video', 'related_case',)

    def view_case_link(self, obj: VideoFile):
        """ A class for creating a table list field with cases related to a videofile.
        """
        case = obj.related_case
        url = (reverse("admin:STO_main_case_changelist")
               + "?" + urlencode({"videofile__id": f"{obj.id}"}))
        return format_html('<a href="{}">{}</a>', url, case)

    view_case_link.short_description = "Связанная работа"


class STOStaffAdmin(admin.ModelAdmin):
    """A class for working with the STOStaff model in the admin panel."""
    list_display = ('id', 'last_name', 'first_name', 'position', 'range', 'is_active')
    list_display_links = ('last_name',)
    search_fields = ('first_name', 'last_name',)
    list_filter = ('is_active',)
    fields = ('is_active', ('last_name', 'first_name',), 'patronymic', 'position',
              'about', ('range', 'image'),)


class CarAdmin(admin.ModelAdmin):
    """A class for working with the Car model in the admin panel."""
    list_display = ('id', 'brand', 'car_model', 'release_year', 'view_case_link', 'is_active',)
    list_display_links = ('brand', 'car_model',)
    search_fields = ('brand', 'car_model', 'vin', 'licence_plate')
    list_filter = ('is_active', 'release_year',)
    fields = ('is_active', ('brand', 'car_model',), ('vin', 'licence_plate',),
              'description', ('release_year', 'mileage'),)

    def view_case_link(self, obj: Car):
        """ A class for creating a table list field with cases related to a car.
        """
        case = obj.case_set.count()
        url = (reverse("admin:STO_main_case_changelist")
               + "?" + urlencode({"car__id": f"{obj.id}"}))
        return format_html('<a href="{}">Связанных заказов: {}</a>', url, case)

    view_case_link.short_description = "Связанные заказы"


class ClientMessagesAdmin(admin.ModelAdmin):
    """A class for working with the Client Messages model in the admin panel."""
    list_display = ('create_time', 'processed', 'user', 'phone',)
    list_display_links = ('user',)
    search_fields = ('user', 'phone',)
    list_filter = ('create_time', 'processed',)
    fields = (('user', 'phone',), 'body', 'chosen_services',
              'processed',)


admin.site.register(Category, CategoryAdmin)
admin.site.register(Case, CaseAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(VideoFile, VideoFileAdmin)
admin.site.register(STOStaff, STOStaffAdmin)
admin.site.register(Stock, StockAdmin)
admin.site.register(Car, CarAdmin)
admin.site.register(ClientMessages, ClientMessagesAdmin)
